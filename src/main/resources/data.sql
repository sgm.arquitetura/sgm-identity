insert into perfil
  (id, codigo,          nome)
values
  (1, 'ADMINISTRADOR',              'Administrador do Sistema'),
  (2, 'CIDADAO',                    'Cidadão'),
  (3, 'OPERADOR_PREFEITURA',        'Operador Prefeitura'),
  (4, 'OPERADOR_ORGAO_MUNICIPAL',   'Operador Órgão Municipal');

insert into usuario
  (id,  nome,               cpf,           email,                   senha,                                                             ativo)
values
  (1,   'Fulano de Tal',    '36740023611', 'admin@sgm.com',    '$2a$10$bKCbF5Z9eGYVIT2tzFLVx.uVrad/tQ2zxUyapMyCvGJVlIzf0fyxO',    true),
  (2,   'Sicrano de Tal',   '98034800228', 'cidadao@sgm.com',   '$2a$10$bKCbF5Z9eGYVIT2tzFLVx.uVrad/tQ2zxUyapMyCvGJVlIzf0fyxO',    true),
  (3,   'Teltrano de Tal',  '16622943362', 'prefeitura@sgm.com',   '$2a$10$bKCbF5Z9eGYVIT2tzFLVx.uVrad/tQ2zxUyapMyCvGJVlIzf0fyxO',    true);

insert into usuario_perfis
  (usuario_id, perfis_id)
values
  (1,   1),
  (2,   2),
  (3,   3);
