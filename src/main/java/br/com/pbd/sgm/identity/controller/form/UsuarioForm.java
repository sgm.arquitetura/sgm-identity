package br.com.pbd.sgm.identity.controller.form;

import br.com.pbd.sgm.identity.modelo.CodigoPerfil;
import br.com.pbd.sgm.identity.modelo.Perfil;
import br.com.pbd.sgm.identity.modelo.Usuario;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public abstract class UsuarioForm {

    @NotNull @NotBlank @Length(min = 11,max = 11, message = "deve ter 11 dígitos")
    private String cpf;

    @NotNull @NotBlank
    private String nome;

    @NotNull @NotBlank
    private String email;

    private boolean ativo;

    private List<CodigoPerfil> perfis;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<CodigoPerfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<CodigoPerfil> perfis) {
        this.perfis = perfis;
    }

    public Usuario toEntity() {
        Usuario usuario = new Usuario();
        usuario.setNome(this.nome);
        usuario.setCpf(this.cpf);
        usuario.setEmail(this.email);
        usuario.setAtivo(this.ativo);
        usuario.setPerfis(perfis.stream().map(p -> new Perfil(p.getId())).collect(Collectors.toList()));
        return usuario;
    }
}
