package br.com.pbd.sgm.identity.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class RespostaErroDTO {

    private final List<ErroDTO> erros = new ArrayList<>();

    public RespostaErroDTO() { }

    public RespostaErroDTO(ErroDTO erro) { adicionar(erro); }

    public RespostaErroDTO(List<ErroDTO> erros) { erros.forEach(this::adicionar); }

    private void adicionar(ErroDTO erro){
        this.erros.add(erro);
    }

    public List<ErroDTO> getErros() {
        return erros;
    }
}
