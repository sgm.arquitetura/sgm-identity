package br.com.pbd.sgm.identity.controller.form;

import br.com.pbd.sgm.identity.modelo.Usuario;

public class UsuarioAtualizacaoForm extends UsuarioForm {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Usuario toEntity() {
        Usuario usuario = super.toEntity();
        usuario.setId(this.id);
        return usuario;
    }
}
