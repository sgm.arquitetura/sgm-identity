package br.com.pbd.sgm.identity.controller.dto;

import br.com.pbd.sgm.identity.modelo.CodigoPerfil;
import br.com.pbd.sgm.identity.modelo.Usuario;

import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;

public class UsuarioDetalheDTO extends UsuarioDTO {

    private List<CodigoPerfil> perfis = new ArrayList<>();

    public UsuarioDetalheDTO() { }

    public UsuarioDetalheDTO(Usuario usuario) {
        super(usuario);
        ofNullable(usuario.getPerfis()).ifPresent(listaPerfis ->
            listaPerfis
                .forEach(perfil -> this.perfis.add(CodigoPerfil.findById(perfil.getId()))));
    }

    public List<CodigoPerfil> getPerfis() {
        return perfis;
    }
}
