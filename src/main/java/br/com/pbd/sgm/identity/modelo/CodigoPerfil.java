package br.com.pbd.sgm.identity.modelo;

import java.util.Arrays;

public enum CodigoPerfil {
    ADMINISTRADOR(1),
    CIDADAO(2),
    OPERADOR_PREFEITURA(3),
    OPERADOR_ORGAO_MUNICIPAL(4);

    private Long id;

    CodigoPerfil(Integer id) {
        this.id = id.longValue();
    }

    public Long getId() {
        return id;
    }

    public static CodigoPerfil findById(Long id){
        return Arrays.stream(CodigoPerfil.values())
                .filter(c -> c.id.equals(id))
                .findFirst()
                .orElse(null);
    }
}
