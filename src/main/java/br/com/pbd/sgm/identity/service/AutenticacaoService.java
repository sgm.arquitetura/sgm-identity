package br.com.pbd.sgm.identity.service;

import br.com.pbd.sgm.identity.modelo.Usuario;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AutenticacaoService implements UserDetailsService {

    private final UsuarioService usuarioService;

    public AutenticacaoService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Usuario> optional = usuarioService.buscarPorEmail(email);
        if(optional.isPresent()){ return optional.get();}
        throw new UsernameNotFoundException("Dados de autenticação inválidos");
    }
}
