package br.com.pbd.sgm.identity.service;

import br.com.pbd.sgm.identity.exception.RecursoNaoEncontradoException;
import br.com.pbd.sgm.identity.exception.ValidacaoNegocioException;
import br.com.pbd.sgm.identity.modelo.Usuario;
import br.com.pbd.sgm.identity.repository.UsuarioRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    public Page<Usuario> buscar(Pageable paginacao) {
        return usuarioRepository.findAll(paginacao);
    }

    public Usuario buscarPorId(Long id) {
        Optional<Usuario> optional = usuarioRepository.findById(id);
        if(optional.isPresent()) { return optional.get();}
        throw new RecursoNaoEncontradoException(id);
    }

    public Optional<Usuario> buscarPorEmail(String email){
        return usuarioRepository.findByEmail(email);
    }

    @Transactional
    public Usuario incluir(Usuario usuario) {
        return salvar(usuario);
    }

    @Transactional
    public Usuario alterar(Usuario usuario) {
        Optional<Usuario> optional = usuarioRepository.findById(usuario.getId());
        if(optional.isPresent()) {
            usuario.setSenha(optional.get().getSenha());
            return salvar(usuario);
        }
        throw new RecursoNaoEncontradoException(usuario.getId());
    }

    private Usuario salvar(Usuario usuario) {
        validar(usuario);
        return usuarioRepository.save(usuario);
    }

    private void validar(Usuario usuario) {
        validarEmailExistente(usuario);
        validarCpfExistente(usuario);
    }

    private void validarCpfExistente(Usuario usuario) {
        Optional<Usuario> optional = usuarioRepository.findByCpf(usuario.getCpf());
        if(optional.isPresent()) {
            if(!optional.get().getId().equals(usuario.getId())){
                throw new ValidacaoNegocioException("Já existe um usuário cadastrado com o cpf informado");
            }
        }
    }

    private void validarEmailExistente(Usuario usuario) {
        Optional<Usuario> optional = usuarioRepository.findByEmail(usuario.getEmail());
        if(optional.isPresent()) {
            if(!optional.get().getId().equals(usuario.getId())){
                throw new ValidacaoNegocioException("Já existe um usuário cadastrado com o email informado");
            }
        }
    }
}
