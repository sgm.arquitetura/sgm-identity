package br.com.pbd.sgm.identity.config;

import br.com.pbd.sgm.identity.controller.dto.ErroDTO;
import br.com.pbd.sgm.identity.controller.dto.ErroFormularioDTO;
import br.com.pbd.sgm.identity.controller.dto.RespostaErroDTO;
import br.com.pbd.sgm.identity.exception.RecursoNaoEncontradoException;
import br.com.pbd.sgm.identity.exception.ValidacaoNegocioException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class IdentityErroHandler {

    private final MessageSource messageSource;

    public IdentityErroHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RespostaErroDTO handleErroFormulario(MethodArgumentNotValidException exception) {
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        List<ErroDTO> erros = fieldErrors.stream()
                .map(fieldError -> {
                    String message = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
                    return new ErroFormularioDTO(fieldError.getField(), message);
                })
                .collect(Collectors.toList());
        return new RespostaErroDTO(erros);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RecursoNaoEncontradoException.class)
    public RespostaErroDTO handleEntidadeNaoEncontrado(RecursoNaoEncontradoException exception) {
        return new RespostaErroDTO(new ErroDTO(exception.getMessage()));
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidacaoNegocioException.class)
    public RespostaErroDTO handleValidacaoNegocio(ValidacaoNegocioException exception) {
        return new RespostaErroDTO(new ErroDTO(exception.getMessage()));
    }

}
