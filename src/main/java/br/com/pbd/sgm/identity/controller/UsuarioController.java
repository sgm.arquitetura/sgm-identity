package br.com.pbd.sgm.identity.controller;

import br.com.pbd.sgm.identity.controller.dto.UsuarioDTO;
import br.com.pbd.sgm.identity.controller.dto.UsuarioDetalheDTO;
import br.com.pbd.sgm.identity.controller.form.UsuarioAtualizacaoForm;
import br.com.pbd.sgm.identity.controller.form.UsuarioInclusaoForm;
import br.com.pbd.sgm.identity.modelo.Usuario;
import br.com.pbd.sgm.identity.service.UsuarioService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;

import static org.springframework.data.domain.Sort.Direction.DESC;

@RestController
@RequestMapping("/users")
public class UsuarioController {

    private final UsuarioService usuarioService;
    private final PasswordEncoder passwordEncoder;

    public UsuarioController(UsuarioService usuarioService, PasswordEncoder passwordEncoder) {
        this.usuarioService = usuarioService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public Page<UsuarioDTO> buscar(@PageableDefault(sort="id", direction = DESC) Pageable paginacao){
        Page<Usuario> usuarios = usuarioService.buscar(paginacao);
        usuarios.map(UsuarioDTO::new);
        return usuarios.map(UsuarioDTO::new);
    }

    @GetMapping("/{id}")
    public UsuarioDetalheDTO buscarPorId(@PathVariable Long id){
        Usuario usuario = usuarioService.buscarPorId(id);
        return new UsuarioDetalheDTO(usuario);
    }

    @PostMapping
    public ResponseEntity<UsuarioDTO> cadastrar(@RequestBody @Valid UsuarioInclusaoForm form, UriComponentsBuilder uriBuilder) {
        Usuario usuario = usuarioService.incluir(form.toEntity(passwordEncoder));
        URI uri = uriBuilder.path("/users/{id}").buildAndExpand(usuario.getId()).toUri();
        return ResponseEntity.created(uri).body(new UsuarioDetalheDTO(usuario));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UsuarioDTO> alterar(@RequestBody @Valid UsuarioAtualizacaoForm form, @PathVariable Long id) {
        form.setId(id);
        Usuario usuario = usuarioService.alterar(form.toEntity());
        return ResponseEntity.ok(new UsuarioDTO(usuario));
    }

    @GetMapping("/auth")
    public UsuarioDetalheDTO buscarAutenticado(Principal user){
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) user;
        Authentication authentication = oAuth2Authentication.getUserAuthentication();
        Usuario principal = (Usuario) authentication.getPrincipal();
        return new UsuarioDetalheDTO(principal);
    }

    @GetMapping("/principal")
    public Principal buscarPrincipal(Principal user) {
        return user;
    }

}
