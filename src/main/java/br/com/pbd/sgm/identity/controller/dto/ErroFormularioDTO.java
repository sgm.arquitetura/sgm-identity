package br.com.pbd.sgm.identity.controller.dto;

public class ErroFormularioDTO extends ErroDTO {

    private final String campo;

    public ErroFormularioDTO(String campo, String erro) {
        super(erro);
        this.campo = campo;
    }

    public String getCampo() {
        return campo;
    }

}
