package br.com.pbd.sgm.identity.controller.dto;

import br.com.pbd.sgm.identity.modelo.Usuario;

public class UsuarioDTO {

    private Long id;
    private String nome;
    private String cpf;
    private String email;
    private boolean ativo;

    public UsuarioDTO() { }

    public UsuarioDTO(Usuario usuario){
        this.id = usuario.getId();
        this.nome = usuario.getNome();
        this.cpf = usuario.getCpf();
        this.email = usuario.getEmail();
        this.ativo = usuario.isAtivo();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEmail() {
        return email;
    }

    public boolean isAtivo() {
        return ativo;
    }

}
