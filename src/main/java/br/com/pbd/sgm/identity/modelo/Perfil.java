package br.com.pbd.sgm.identity.modelo;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
public class Perfil implements GrantedAuthority {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private CodigoPerfil codigo;
    private String nome;

    public Perfil() {
    }

    public Perfil(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CodigoPerfil getCodigo() {
        return codigo;
    }

    public void setCodigo(CodigoPerfil codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getAuthority() {
        return codigo.name();
    }
}
