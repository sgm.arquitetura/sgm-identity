package br.com.pbd.sgm.identity.controller.dto;

public class ErroDTO {

    private final String erro;

    public ErroDTO(String erro) {
        this.erro = erro;
    }

    public String getErro() {
        return erro;
    }
}