package br.com.pbd.sgm.identity.controller.form;

import br.com.pbd.sgm.identity.modelo.Usuario;
import org.springframework.security.crypto.password.PasswordEncoder;

import static java.util.Objects.nonNull;

public class UsuarioInclusaoForm extends UsuarioForm {

    private String senha;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Usuario toEntity(PasswordEncoder passwordEncoder) {
        Usuario usuario = super.toEntity();

        //TODO Remover posteriormente, adicionado geração de senha apenas para facilitar o teste da api
        if(nonNull(this.senha)) {
            usuario.setSenha(passwordEncoder.encode(this.senha));
        } else {
            usuario.setSenha(passwordEncoder.encode("123456"));
        }
        return usuario;
    }

}
