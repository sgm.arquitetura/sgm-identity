package br.com.pbd.sgm.identity.exception;

public class ValidacaoNegocioException extends RuntimeException {

    public ValidacaoNegocioException(String message) {
        super(message);
    }

}
