package br.com.pbd.sgm.identity.controller;

import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/oauth")
public class OAuthController {

    private final ConsumerTokenServices tokenServices;

    public OAuthController(ConsumerTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @GetMapping("/logout")
    public void logout(@RequestHeader("Authorization") String authorization) {
        String clientId = authorization.replaceAll("(?i)bearer", "").trim();
        tokenServices.revokeToken(clientId);
    }

    @GetMapping("/me")
    public Principal buscarPrincipal(Principal user) {
        return user;
    }

}
