package br.com.pbd.sgm.identity.exception;

public class RecursoNaoEncontradoException extends RuntimeException {

    public RecursoNaoEncontradoException(Long id) {
        super(String.format("Recurso %d não encontrado", id));
    }
}
